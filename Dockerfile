FROM docker:stable
# Install AWS CLI and docker-compose
RUN apk add --no-cache python3\
  && pip3 install awscli
RUN apk add --no-cache openssh ca-certificates bash jq