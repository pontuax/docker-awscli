# docker-node-awscli
Dockerfile for the pontuax/docker-awscli image

@Dockerhub: https://hub.docker.com/r/pontuax/docker-awscli/

## Pull image

``` bash
docker pull pontuax/docker-awscli:latest

```

## What is installed in this image?
* docker 18.06 (stable)
* awscli tool via pip

## Usage

We use this image as base for our bitbucket pipelines config, but feel free to use it wherever you feel it is suited.